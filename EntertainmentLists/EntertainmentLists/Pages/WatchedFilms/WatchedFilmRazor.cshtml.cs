using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EntertainmentLists.Pages.WatchedFilms
{
    public class WatchedFilmRazorModel : PageModel
    {
        private readonly IFilmService<WatchedFilm> _watchedFilmService;
        public IEnumerable<WatchedFilm> watchedFilmModels { get; set; }
        public WatchedFilmRazorModel(IFilmService<WatchedFilm> watchedFilmService)
        {
            _watchedFilmService = watchedFilmService;
        }
        public void OnGetAsync()
        {
            watchedFilmModels = _watchedFilmService.GetAllFilmsFromDb();
        }
        public IActionResult OnPostDeleteAsync(int id)
        {
            _watchedFilmService.RemoveFilmFromDb(id);
            return RedirectToPage();
        }
    }
}