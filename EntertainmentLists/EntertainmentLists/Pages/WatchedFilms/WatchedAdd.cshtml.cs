using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EntertainmentLists.Pages.WatchedFilms
{
    public class WatchedAddModel : PageModel
    {
        private readonly IFilmService<WatchedFilm> _watchedFilmService;
        [BindProperty]
        public WatchedFilm WatchedFilm { get; set; }
        public WatchedAddModel(IFilmService<WatchedFilm> watchedFilmService)
        {
            _watchedFilmService = watchedFilmService;
        }
        public IEnumerable<WatchedFilm> watchedFilmModels { get; set; }
        public void OnGet()
        {
            watchedFilmModels = _watchedFilmService.GetAllFilmsFromDb();
        }
        public IActionResult OnPost()
        {
            _watchedFilmService.SaveNewFilmToDb(WatchedFilm);
            return RedirectToPage("/WatchedFilms/WatchedFilmRazor");
        }
    }
}