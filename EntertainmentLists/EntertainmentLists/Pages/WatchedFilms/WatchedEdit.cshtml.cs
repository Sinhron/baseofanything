using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EntertainmentLists.Pages.WatchedFilms
{
    public class WatchedEditModel : PageModel
    {
        private readonly IFilmService<WatchedFilm> _watchedFilmService;
        public WatchedEditModel(IFilmService<WatchedFilm> watchedFilmService)
        {
            _watchedFilmService = watchedFilmService;
        }
        public IEnumerable<WatchedFilm> watchedFilmModels { get; set; }
        public void OnGet()
        {
            watchedFilmModels = _watchedFilmService.GetAllFilmsFromDb();
        }
        public IActionResult OnPost(WatchedFilm watchedFilm)
        {
            _watchedFilmService.ChangeFilmPropertiesInDb(watchedFilm);
            return RedirectToPage("/WatchedFilms/WatchedFilmRazor");
        }
    }
}