﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace EntertainmentLists.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IFilmService<WatchedFilm> _watchedFilmService;
        private readonly IFilmService<AwaitingFilm> _awaitingFilmService;
        public int amountWatchedFilms { get; set; }
        public int amountAwaitingFilms { get; set; }

        public IndexModel(IFilmService<WatchedFilm> watchedFilmService, IFilmService<AwaitingFilm> awaitingFilmService)
        {
            _watchedFilmService = watchedFilmService;
            _awaitingFilmService = awaitingFilmService;
        }

        public void OnGet()
        {
            amountWatchedFilms = _watchedFilmService.GetAllFilmsFromDb().Count();
            amountAwaitingFilms = _awaitingFilmService.GetAllFilmsFromDb().Count();
        }
    }
}
