using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EntertainmentLists.Pages.AwaitingFilms
{
    public class AwaitingEditModel : PageModel
    {
        private readonly IFilmService<AwaitingFilm> _awaitingFilmService;
        public IEnumerable<AwaitingFilm> awaitingFilmModels { get; set; }
        public AwaitingEditModel(IFilmService<AwaitingFilm> awaitingFilmService)
        {
            _awaitingFilmService = awaitingFilmService;
        }
        public void OnGet()
        {
            awaitingFilmModels = _awaitingFilmService.GetAllFilmsFromDb();
        }
        public IActionResult OnPost(AwaitingFilm awaitingFilm)
        {
            _awaitingFilmService.ChangeFilmPropertiesInDb(awaitingFilm);
            return RedirectToPage("/AwaitingFilms/AwaitingFilmRazor");
        }
    }
}