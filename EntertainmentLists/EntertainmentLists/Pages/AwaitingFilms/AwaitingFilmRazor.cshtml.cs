using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EntertainmentLists.Pages.AwaitingFilms
{
    public class AwaitingFilmRazorModel : PageModel
    {
        private readonly IFilmService<AwaitingFilm> _awaitingFilmService;
        public AwaitingFilmRazorModel(IFilmService<AwaitingFilm> awaitingFilmService)
        {
            _awaitingFilmService = awaitingFilmService;
        }
        public IEnumerable<AwaitingFilm> awaitingFilmModels { get; set; }
        public void OnGet()
        {
            awaitingFilmModels = _awaitingFilmService.GetAllFilmsFromDb();
        }
        public IActionResult OnPostDelete(int id)
        {
            _awaitingFilmService.RemoveFilmFromDb(id);
            return RedirectToPage();
        }
    }
}