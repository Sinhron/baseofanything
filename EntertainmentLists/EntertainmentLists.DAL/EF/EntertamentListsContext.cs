﻿using EntertamentLists.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntertamentLists.DAL.EF
{
    public class EntertamentListsContext : DbContext
    {
        public EntertamentListsContext(DbContextOptions<EntertamentListsContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<WatchedFilm> WatchedFilms { get; set; }
        public DbSet<AwaitingFilm> AwaitingFilms { get; set; }

        //public DbSet<TVSeries> Serials { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WatchedFilm>().HasData(
                new WatchedFilm() { Id = 1, Name = "Властелин колец", Rating = 10 },
                new WatchedFilm() { Id = 2, Name = "Назад в будущее", Rating = 10 });

            modelBuilder.Entity<AwaitingFilm>().HasData(
               new AwaitingFilm() { Id = 1, Name = "Тор 4" });
        }
    }
}
