﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EntertamentLists.DAL.Entities
{
    public class AwaitingFilm 
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        public AwaitingFilm() { }
        public AwaitingFilm(string name)
        {
            Name = name;
        }
    }
}
