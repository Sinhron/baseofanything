﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EntertamentLists.DAL.Entities
{
    public class WatchedFilm 
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Rating is required.")]
        [Range(0, 10, ErrorMessage = "Incorrect rating")]
        public int Rating { get; set; }
        public WatchedFilm() { }
        public WatchedFilm(string name, int rating)
        {
            Name = name;
            Rating = rating;
        }
    }
}
