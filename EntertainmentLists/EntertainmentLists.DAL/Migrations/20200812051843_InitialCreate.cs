﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EntertainmentLists.DAL.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AwaitingFilms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AwaitingFilms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WatchedFilms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WatchedFilms", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "AwaitingFilms",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Тор 4" });

            migrationBuilder.InsertData(
                table: "WatchedFilms",
                columns: new[] { "Id", "Name", "Rating" },
                values: new object[] { 1, "Властелин колец", 10 });

            migrationBuilder.InsertData(
                table: "WatchedFilms",
                columns: new[] { "Id", "Name", "Rating" },
                values: new object[] { 2, "Назад в будущее", 10 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AwaitingFilms");

            migrationBuilder.DropTable(
                name: "WatchedFilms");
        }
    }
}
