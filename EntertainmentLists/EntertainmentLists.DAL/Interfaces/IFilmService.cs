﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EntertainmentLists.DAL.Interfaces
{
    public interface IFilmService<T> where T : class
    {
        void SaveNewFilmToDb(T film);
        List<T> GetAllFilmsFromDb();
        T GetFilmByIdFromDb(int id);
        void ChangeFilmPropertiesInDb(T film);
        void RemoveFilmFromDb(int id);
    }
}