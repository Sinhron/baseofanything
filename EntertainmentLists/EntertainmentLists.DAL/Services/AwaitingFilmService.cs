﻿using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.EF;
using EntertamentLists.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntertainmentLists.DAL.Services
{
    public class AwaitingFilmService : IFilmService<AwaitingFilm>
    {
        private readonly EntertamentListsContext _db;
        public AwaitingFilmService(EntertamentListsContext context)
        {
            _db = context;
        }

        public void SaveNewFilmToDb(AwaitingFilm film)
        {
            _db.AwaitingFilms.Add(film);
            _db.SaveChanges();
        }

        public List<AwaitingFilm> GetAllFilmsFromDb()
        {
            return _db.AwaitingFilms.AsNoTracking().ToList();
        }

        public AwaitingFilm GetFilmByIdFromDb(int id)
        {
            return _db.AwaitingFilms.FirstOrDefault(film => film.Id == id);
        }

        public void ChangeFilmPropertiesInDb(AwaitingFilm film)
        {
            _db.Entry(film).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void RemoveFilmFromDb(int id)
        {
            var film = _db.AwaitingFilms.Find(id);
            if (film is null)
            {
                throw new ArgumentException("Awaiting film has been not found");
            }
            _db.AwaitingFilms.Remove(film);
            _db.SaveChanges();
        }

       
    }
}
