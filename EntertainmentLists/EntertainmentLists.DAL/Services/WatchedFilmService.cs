﻿using EntertainmentLists.DAL.Interfaces;
using EntertamentLists.DAL.EF;
using EntertamentLists.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EntertainmentLists.DAL.Services
{
    public class WatchedFilmService : IFilmService<WatchedFilm>
    {
        private readonly EntertamentListsContext _db;
        public WatchedFilmService(EntertamentListsContext context)
        {
            _db = context;
        }

        public void SaveNewFilmToDb(WatchedFilm film)
        {
            _db.WatchedFilms.Add(film);
            _db.SaveChanges();
        }

        public  List<WatchedFilm> GetAllFilmsFromDb()
        {
            return _db.WatchedFilms.AsNoTracking().ToList();
        }

        public WatchedFilm GetFilmByIdFromDb(int id)
        {
            return _db.WatchedFilms.FirstOrDefault(film => film.Id == id);
        }
        public void ChangeFilmPropertiesInDb(WatchedFilm film)
        {
            _db.Entry(film).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void RemoveFilmFromDb(int id)
        {
            var film = _db.WatchedFilms.Find(id);
            if (film is null)
            {
                throw new ArgumentException("Watched film has been not found");
            }
            _db.WatchedFilms.Remove(film);
            _db.SaveChanges();
        }
    }
}